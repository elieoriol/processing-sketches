//GUI
import controlP5.*;
ControlP5 gui;
Accordion acc;

//Sound
import processing.sound.*;
FFT fft;
AudioIn in;
int bands = 8;
float[] spectrum = new float[bands];
int bd = 0;
float threshold = 0.15;

//Image
PImage img;

//Grid
float gridX = 10, gridY = 10;
float[][] w, h;
color[][] col;

//Grid/wave
float tileDist;
FloatList waveDist = new FloatList(), waveBD = new FloatList();
float MAX_DIST, DEFAULT_WAVE_BD = 150;

//Waves
int MAX_WAVES = 5;
FloatList xog = new FloatList(), yog = new FloatList(), tog = new FloatList();

//Lerp
float lerpFactor;

//Colors
FloatList waveColors = new FloatList();
color blue = color(100,130,228);
color red = color(228,130,100);
color green = color(115, 228, 115);
color[] colors = new color[]{blue, red, green};
color gray = color(192);

//Random Seed
int actRandomSeed;

//Time
float clickTime;
float lastTime;


//////////////////////////
void setup() {
  size(700, 700);
  
  img = loadImage("rose.png");
  img.resize(width, height);
  
  smooth();
  
  //GUI
  gui = new ControlP5(this);
  
  Group g3 = gui.addGroup("myGroup3")
                .setBackgroundColor(color(0, 64))
                .setBackgroundHeight(150);
  
  gui.addBang("bang")
     .setPosition(10,10)
     .setSize(20,20)
     .moveTo(g3);
     
  acc = gui.addAccordion("acc")
           .setPosition(10,10)
           .setWidth(40)
           .addItem(g3);
           
  gui.mapKeyFor(new ControlKey() {public void keyEvent() {acc.open(0);}}, 'o');
  gui.mapKeyFor(new ControlKey() {public void keyEvent() {acc.close(0);}}, 'c');
  
  
  MAX_DIST = 2*sqrt(pow(width/2,2) + pow(height/2,2)); //+ waveBD;
  
  //Initialize w, h, col
  int wg = int(width/gridX)+1, hg = int(height/gridX)+1;
  w = new float[wg][hg];
  h = new float[wg][hg];
  col = new color[wg][hg];
  for (int i = 0; i < width/gridX; i++) {
    for (int j = 0; j < height/gridY; j++) {
      //PImage newImg = img.get(int(i*gridX), int(j*gridY), int(gridX), int(gridY));
      //col[i][j] = extractColorFromImage(newImg);
      col[i][j] = img.pixels[int(j*gridY*width+i*gridX)];
      int r=(col[i][j]&0x00FF0000)>>16;
      int g=(col[i][j]&0x0000FF00)>>8;
      int b=(col[i][j]&0x000000FF);
      int gr = (r+g+b)/3;
      col[i][j] = color(gr);
      if (red(col[i][j]) == 0 && green(col[i][j]) == 0 && blue(col[i][j]) == 0) {
        col[i][j] = color(255);
      }
      
      float sizeX = map(gr, 0, 255, gridX, 1), sizeY = map(gr, 0, 255, gridY, 1);
      
      w[i][j] = sizeX;
      h[i][j] = sizeY;
    }
  }
  
  //Random
  actRandomSeed = 0;
  randomSeed(actRandomSeed);
  
  //Sound
  fft = new FFT(this, bands);
  in = new AudioIn(this, 1);
  
  in.start();
  fft.input(in);
}


void draw() {
  for (int i = 0; i < waveDist.size(); i++) {
    waveDist.set(i, MAX_DIST*(sin(HALF_PI*(millis() - tog.get(i)) / 8000)));
    
    if (waveDist.get(i) > MAX_DIST - 1) {
      waveDist.remove(i); xog.remove(i); yog.remove(i); tog.remove(i); waveBD.remove(i); waveColors.remove(i);
      i--;
    }
  }
  
  fft.analyze(spectrum);
  
  if (spectrum[bd] > threshold && millis() - lastTime > 100) {
    xog.append(random(0,width));
    yog.append(random(0,height));
    tog.append(millis());
    waveDist.append(0);
    waveBD.append(DEFAULT_WAVE_BD);
    waveColors.append((int)random(0,colors.length));
    lastTime = millis();
  }
  
  background(255);
  noStroke();
  
  translate(gridX/2, gridY/2);
  
  for (int tileX = 0; tileX < width/gridX; tileX++) {
    
    for (int tileY = 0; tileY < height/gridY; tileY++) {
      color tileColor = col[tileX][tileY];
      fill(tileColor);
      
      float ws = w[tileX][tileY], hs = h[tileX][tileY];
      
      for (int k = 0; k < waveDist.size(); k++) {
        
        tileDist = dist(xog.get(k), yog.get(k), tileX*gridX, tileY*gridY);
        float wd = waveDist.get(k), wbd = waveBD.get(k);
        color c = colors[(int)waveColors.get(k)];

        
        if (tileDist >= wd - wbd/2 && tileDist <= wd) {
          lerpFactor = cos(TWO_PI * (wd-tileDist)/wbd);
          fill(lerpColor(tileColor, c, lerpFactor/2));
          //w[tileX][tileY] = lerp(0, gridX, lerpFactor);
          //h[tileX][tileY] = lerp(0, gridY, lerpFactor);
          //ws = lerp(0, w[tileX][tileY], lerpFactor);
          //hs = lerp(0, h[tileX][tileY], lerpFactor);
          ws = lerp(w[tileX][tileY], gridX, lerpFactor);
          hs = lerp(h[tileX][tileY], gridY, lerpFactor);
        }
        
        if (tileDist > wd && tileDist <= wd + wbd/2) {
          lerpFactor = cos(TWO_PI * (tileDist-wd)/wbd);
          fill(lerpColor(tileColor, c, lerpFactor/2));
          //w[tileX][tileY] = lerp(0, gridX, lerpFactor);
          //h[tileX][tileY] = lerp(0, gridY, lerpFactor);
          //ws = lerp(0, w[tileX][tileY], lerpFactor);
          //hs = lerp(0, h[tileX][tileY], lerpFactor);
          ws = lerp(w[tileX][tileY], gridX, lerpFactor);
          hs = lerp(h[tileX][tileY], gridY, lerpFactor);
        }
      }
      
      //ellipse(tileX*gridX, tileY*gridY, w[tileX][tileY], h[tileX][tileY]);
      ellipse(tileX*gridX, tileY*gridY, ws, hs);
      //rect(tileX*gridX, tileY*gridY, w[tileX][tileY], h[tileX][tileY]);
    }
  }
}

/*
void mousePressed() {
  clickTime = millis();
}


void mouseReleased() {
  xog.append(mouseX);
  yog.append(mouseY);
  tog.append(millis());
  waveDist.append(0);
  
  float wbd = max(150, min((millis()-clickTime)/10, 400));
  waveBD.append(wbd);
  
  waveColors.append((int)random(0,colors.length));
}
*/


void bang() {
  xog.append(random(0,width));
  yog.append(random(0,height));
  tog.append(millis());
  waveDist.append(0);
  waveBD.append(DEFAULT_WAVE_BD);
  waveColors.append((int)random(0,colors.length));
}

void keyPressed() {
  if (key == ESC) {
    in.stop();
    exit();
  }
}


color extractColorFromImage(final PImage img) {
  img.loadPixels();
  color r = 0, g = 0, b = 0;
 
  for (final color c : img.pixels) {
    r += c >> 020 & 0xFF;
    g += c >> 010 & 0xFF;
    b += c        & 0xFF;
  }
 
  r /= img.pixels.length;
  g /= img.pixels.length;
  b /= img.pixels.length;
 
  return color(r, g, b);
}