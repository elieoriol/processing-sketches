import processing.sound.*;

FFT fft;
AudioIn in;
int bands = 32;
float[] spectrum = new float[bands];

void setup() {
  size(600,600);
  background(255);
    
  // Create an Input stream which is routed into the Amplitude analyzer
  fft = new FFT(this, bands);
  in = new AudioIn(this, 1);
  
  // start the Audio Input
  in.start();
  
  // patch the AudioIn
  fft.input(in);
}      

void draw() { 
  background(255);
  fill(0);
  fft.analyze(spectrum);

  for(int i = 0; i < bands; i++){
  // The result of the FFT is normalized
  // draw the line for frequency band i scaling it up by 5 to get more amplitude.
    rect(i*width/bands, height - spectrum[i]*height, width/bands, spectrum[i]*height );
  }
  
  if (spectrum[0] > 0.15) {
    ellipse(width -20, 20, 10, 10);
  }
}