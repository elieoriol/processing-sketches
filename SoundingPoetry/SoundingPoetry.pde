PFont font;
float minFontSize = 20, maxFontSize = 200;

String[] txtWords;
int wordIdx = 0;
FloatList wordIdxs = new FloatList();
FloatList fontSizes = new FloatList(), xs = new FloatList(), ys = new FloatList();
FloatList times = new FloatList();

float margin = 30;

float prevTime = 0.;
float aTime = 500, lTime = 0, vTime = 3000;

int actRandomSeed;

void setup() {
  size(800,600);
  
  frameRate(15);
    
  font = createFont("Arial", 10);
  smooth();

  //  ------ read a textfile ------
  String[] lines = loadStrings("baudelaire.txt");
  // join all lines to a big string
  String joinedText = join(lines, " ");

  // replacings
  joinedText = joinedText.replaceAll("_", "");  

  // split tex into words by delimiters
  txtWords = splitTokens(joinedText, " ¬ª¬´‚Äì_-–().,;:?!\u2014\"");
  
  textAlign(LEFT);
  fill(0);
  noStroke();
  
  actRandomSeed = 0;
  randomSeed(actRandomSeed);
}


void draw() {
  background(255);
  
  if (prevTime == 0. || millis() - prevTime > 1000) {
    prevTime = millis();
    times.append(prevTime);
    
    float fontSize = random(minFontSize, maxFontSize);
    textFont(font, fontSize);
    
    float rLimit = width - margin - textWidth(txtWords[wordIdx]);
    while (rLimit <= 0) {
      fontSize = random(minFontSize, maxFontSize);
      textFont(font, fontSize);
      rLimit = width - margin - textWidth(txtWords[wordIdx]);
    }
    
    fontSizes.append(fontSize);
    
    wordIdxs.append(wordIdx);
    
    float x = random(margin, rLimit);
    float y = random(margin + textAscent(), height - margin - textDescent());
    
    boolean ok = false;
    while (!ok) {
      ok = true;
      for (int i = 0; i < xs.size(); i++) {
        textFont(font, fontSizes.get(i));
        if (x > xs.get(i) &&
            x < xs.get(i) + textWidth(txtWords[int(wordIdxs.get(i))]) &&
            y > ys.get(i) - textAscent() &&
            y < ys.get(i) + textDescent()) {
          x = random(margin, rLimit);
          y = random(margin + textAscent(), height - margin - textDescent());
          ok = false;
          break;
        }
      }
    }
    
    xs.append(x);
    ys.append(y);
    
    wordIdx++;
  }
  
  for (int i = 0; i < wordIdxs.size(); i++) {
    float delta = millis() - times.get(i);
    
    if (delta > aTime + lTime + vTime) {
      times.remove(i);
      fontSizes.remove(i);
      wordIdxs.remove(i);
      xs.remove(i); ys.remove(i);
      i--; continue;
    }
    
    if (delta < aTime) {
      fill( map(delta, 0, aTime, 255, 0) );
    }
    else if (delta < aTime + lTime) {
      fill(0);
    }
    else {
      fill( map(delta, aTime + lTime, aTime + lTime + vTime, 0, 255) );
    }
    
    textFont(font, fontSizes.get(i));
    
    text(txtWords[int(wordIdxs.get(i))], xs.get(i), ys.get(i));
  }
}