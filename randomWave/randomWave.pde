float gridX = 10, gridY = 10;
float w, h;
int waveBD = 150;
float waveDist, tileDist;
float MAX_DIST;
float xog, yog;

float lerpFactor;
color blue = color(100,130,228);
color gray = color(128);

int actRandomSeed;

void setup() {
  size(600, 600);
  MAX_DIST = sqrt(pow(width/2,2) + pow(height/2,2)); //+ waveBD;
  smooth();
  
  //actRandomSeed = 0;
  //randomSeed(actRandomSeed);
}

void draw() {
  waveDist = MAX_DIST*(cos(HALF_PI*millis()/2000)+1)/2 - waveBD/2;
  
  if (waveDist < -waveBD/2 + 1) {
    xog = random(0, width);
    yog = random(0, height);
  }
  
  background(255);
  noStroke();
  
  translate(gridX/2, gridY/2);
  
  for (int tileX = 0; tileX < width/gridX; tileX++) {
    //fillVal = 191-128*cos(TWO_PI*millis()/1000);
    //fill(fillVal*tileX*gridX/width, 0, 0);
    for (int tileY = 0; tileY < height/gridX; tileY++) {
      fill(gray);
      w = gridX; h = gridY;
      
      tileDist = dist(xog, yog, tileX*gridX, tileY*gridY);
      if (tileDist >= waveDist - waveBD/2 && tileDist <= waveDist) {
        lerpFactor = cos(TWO_PI * (waveDist-tileDist)/waveBD);
        fill(lerpColor(gray, blue, lerpFactor/2));
        w = lerp(0, gridX, lerpFactor);
        h = lerp(0, gridY, lerpFactor);
      }
      if (tileDist > waveDist && tileDist <= waveDist + waveBD/2) {
        lerpFactor = cos(TWO_PI * (tileDist-waveDist)/waveBD);
        fill(lerpColor(gray, blue, lerpFactor/2));
        w = lerp(0, gridX, lerpFactor);
        h = lerp(0, gridY, lerpFactor);
      }
      
      ellipse(tileX*gridX, tileY*gridY, w, h);
    }
  }
}